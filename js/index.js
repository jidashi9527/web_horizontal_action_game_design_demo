window,onload=function(){
//定义位置
var x = 30;
var y = 0;
//定义速度
var speed = 1;
//定义两个循环计时器
var right_timer;
var left_timer;
//定义玩家变量
function right(){
	clearInterval(right_timer);
	right_timer = setInterval(function(){
		if(x == 500){
			return false;
		}
		$('#player').css("left",x + speed +"px");
		x += speed;
	},2);
}
function left(){
	clearInterval(left_timer);
	left_timer = setInterval(function(){
		if(x == 0){
			return false;
		}
		$('#player').css("left",x - speed +"px");
		x -= speed;
	},2);
}
//定义两个循环计时器
$(window).keydown(function(event){
  switch(event.keyCode) {
	case 68:
		if(x !== 300){
			right();
		}
		break;
	case 65:
		left();
		break;
	default:
		break;
  }
});
$(window).keyup(function(event){
  switch(event.keyCode) {
	case 68:
		clearInterval(right_timer);
		break;
	case 65:
		clearInterval(left_timer);
		break;
	default:
		break;
  }
});
};